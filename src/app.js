const express = require('express');
const app = express();

const run = () => {
  app.listen (3000, () => { });
};

module.exports = run;